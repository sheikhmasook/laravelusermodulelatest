<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    
    'user' => [
        'login' => 'Login Successfully.',
        'otp' => 'OTP does not match.',
        'inactive_account' => 'Your account is inactive, please contact to administrator.',
        'invalid_mobile_no'=>'Invalid Mobile Number',
        'otp_send'=>'OTP send Successfully',
        'profile_updated'=>'Profile updated successfully',
        'unable_update'=>'unable to update profile',
        'logout'=>'Logout successfully',
        'signup'=>'User Registration successfully',
        'invalid_mobile_otp'=>'Invalid Mobile Number or OTP does not match'
    ],
    
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'multiple_of' => 'The :attribute must be a multiple of :value.',


];
