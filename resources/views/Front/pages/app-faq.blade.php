   @extends('layouts.front.app')
   @section('content')
   <div class="body_section faq-body-section">
    <div class="top-padding pt-3">
      <!-- Contact Section -->
      <section class="aboutus-sec inner-page-padding py-0 set-accordian">
        <div class="container-fluid">
           <div class="text-center position-relative">
          <!-- FAQ Content -->
          <div class="faq-content">
            <div id="accordion" class="accordian-collapse">
              @php $counter = 1;@endphp
              @foreach ($row as $key => $value)
               @php $accordion = ($key == 0) ? 'show' : "";@endphp
              <div class="card">
                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne_{{ $key }}" aria-expanded="true" aria-controls="collapseOne_{{ $key }}">
                  <h5 class="mb-0 d-flex align-items-center text-left">
                    {{ $counter.') '.$value['title']}} <span class="ml-auto icon icon-icon-plus-ic"></span>
                  </h5>
                </div>

                <div id="collapseOne_{{ $key }}" class="collapse {{ $accordion }}" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body text-left">
                    {!! $value['description'] !!}
                  </div>
                </div>
              </div>
              @php $counter++;@endphp
              @endforeach
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function(){
    // Add minus icon for collapse element which is open by default
    $(".collapse.show").each(function(){
      $(this).prev(".card-header").find(".icon").addClass("icon-icon-minus-ic").removeClass("icon-icon-plus-ic");
    });

    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function(){
      $(this).prev(".card-header").find(".icon").removeClass("icon-icon-plus-ic").addClass("icon-icon-minus-ic");
    }).on('hide.bs.collapse', function(){
      $(this).prev(".card-header").find(".icon").removeClass("icon-icon-minus-ic").addClass("icon-icon-plus-ic");
    });
  });
</script>

@endsection