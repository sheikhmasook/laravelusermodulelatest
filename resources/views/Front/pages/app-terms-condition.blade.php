   @extends('layouts.front.app')
   @section('content')
<div class="body_section">
      <div class="top-padding pt-3">
        <!-- Contact Section -->
        <section class="aboutus-sec inner-page-padding py-0">
          <div class="container-fluid">
           <div class="text-center position-relative">
            <!-- About Us Content -->
            <div class="about-us-content mt-3">
              <div class="mb-5">
              {!! $row['description'] !!}
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    @endsection