<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="csrf-token" content="{{ csrf_token() }}" />


@php
$current_page=Request::segment(1);
$current_page2=Request::segment(2);
@endphp
@if($current_page == 'pages')
   <title><?php echo config('app.name') ?> | {{ ucfirst(@$current_page2) }}</title>
 @else
  <title><?php echo config('app.name') ?> | {{ ucfirst(@$current_page) }}</title>
 @endif
</head>
<body>

@yield('content')

</body>
</html>
