<style type="text/css">
    @font-face {
        font-family: 'nowaymedium';
        src: url("{{ url('/') }}/public/css/admin/noway-medium-webfont.woff2") format('woff2'),
        url("{{ url('/') }}/public/css/admin/noway-medium-webfont.woff") format('woff');
        font-weight: normal;
        font-style: normal;

    }

    @import url('https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900&display=swap');
</style>
<title>{{ setting('site-title') }}</title>
<table align="center" style="width: 100%; max-width: 706px; border-collapse: collapse; border:1px solid #dddddd;">
    <tbody>
        <tr style="background: #FFFFFF;">
            <td style="padding: 15px 0 15px 78px;"><a href="{{ url('/') }} " style="display: block;"><img
                        src="{{ url(setting('site-logo'))}}" style="outline:none; width:96px;"></a></td>
        </tr>
        <tr>
            @yield('content')
        </tr>
        <tr>
            <td colspan="2" style="background: #C8C8C8; padding: 0px 78px 70px 78px;">
                <table
                    style="width: 100%;border-collapse: collapse; background: #E5E5E5; padding: 30px 75px 40px 75px; text-align: center;">
                    <tbody>
                        <tr style="">
                            <td colspan="2"
                                style=" font-family: 'nowaymedium'; font-size:11px;  color:#242424;  padding: 40px 75px 10px 75px;">
                                Download the {{ setting('company-name')}} App now</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-right: 4px;"><a download=""
                                    href="{{ setting('android-link	')}}"><img alt=""
                                        src="{{ url('/')}}/public/img/e_android.png" style="width: 118px;"></a></td>
                            <td style="text-align: left; padding-left: 4px;"><a download=""
                                    href="{{ setting('ios-link')}}"><img alt="" src="{{ url('/')}}/public/img/e_ios.png"
                                        style="width: 118px;"></a></td>
                        </tr>
                        <tr style="">
                            <td colspan="2"
                                style=" font-family: 'nowaymedium'; font-size:11px;  color:#242424;  padding: 0px 75px 10px 75px;">
                                Follow us</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-right: 4px;"><a
                                    href="{{ setting('facebook-link')}}"><img alt=""
                                        src="{{ url('/')}}/public/img/e_facebook.png" style="width: 20px;"></a></td>
                            <td style="text-align: left; padding-left: 4px;"><a
                                    href="{{ setting('instagram-link')}}"><img alt=""
                                        src="{{ url('/')}}/public/img/e_instagram.png" style="width: 20px;"></a>
                            </td>
                        </tr>

                        <tr>
                            <td align="center" colspan="2" style="padding: 33px 0px 14px 0px;"><a href="#"
                                    style="font-size: 10px; color: #A1A1A1; font-weight: 600; text-decoration: none;">©&nbsp;</a><a
                                    href="#"
                                    style="background-color: rgb(229, 229, 229); letter-spacing: 0.05em; color: rgb(161, 161, 161);">{{ setting('company-name')}}</a><br>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>