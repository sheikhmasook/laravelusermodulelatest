<table>
    <thead>
        <tr>
            <th>Id</th>
            <th width="15">Name</th>
            <th width="20">Email</th>
            <th width="10">Phone Number</th>
            <th width="10">Status</th>
            <th width="20">Created on</th>
        <tr>
    </thead>
    <tbody>
        @foreach ($data as $key=>$user)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->phone_no }}</td>
            <td>{{ $user->status == 1 ? "Active" : "Deactive" }}</td>
            <td>{{ $user->created_at }}</td>
        </tr>    
        @endforeach
    </tbody>
</table>
