<div class=" p-4">
    <div class="form-row">
        <div class="form-group col-md-6">
            <label>First Name</label>
            {{ Form::text('first_name', null, ['placeholder' => 'First Name', 'id' => 'first_name', 'class' => 'form-control']) }}
        </div>
        <div class="form-group col-md-6">
            <label>Last Name</label>
            {{ Form::text('last_name', null, ['placeholder' => 'Last Name', 'id' => 'last_name', 'class' => 'form-control']) }}
        </div>

        <div class="form-group col-md-6">
            <label>Email</label>
            {!! Form::text('email', null, ['placeholder' => 'Email', 'id' => 'email', 'class' => 'form-control']) !!}
        </div>

        <div class="form-group col-md-6">
            <label>Phone Number</label>
            {!! Form::text('phone_no', null, ['placeholder' => 'Phone Number', 'id' => 'phone_no', 'class' => 'form-control']) !!}
        </div>
    
        <div class="form-group col-md-12">
            <label>Description </label>
            {!! Form::textarea('description', null, ['placeholder' => 'Description', 'id' => 'description', 'class' => 'form-control']) !!}
        </div>

        <div class="form-group col-md-12">
            <label>Image</label>
            @if (isset($id))
                <input type="file" id="profile_picture"
                    data-default-file="{{ $getuser->profile_picture && file_exists($getuser->profile_picture) ? url($getuser->profile_picture) : '' }}"
                    name="profile_picture" class="dropify" data-height="150" data-show-remove="false"
                    data-allowed-file-extensions="png jpeg jpg gif" />
            @else
                <input type="file" id="profile_picture" name="profile_picture" class="dropify" data-height="150"
                    data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif" />
            @endif
        </div>

    </div>
</div>
