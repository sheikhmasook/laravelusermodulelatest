@extends('layouts.admin.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg-12">
                        @if($data->profile_picture!="" && file_exists($data->profile_picture))
                        <div class="avatar text-center">
                        <img src="{{url($data->profile_picture)}}" alt="" class="img-thumbnail" style="max-height:150px;">
                        </div>
                        @endif
                    <h3 class="text-center" style="margin-top: 5px;">{{@($data->full_name)?$data->full_name:''}}</h3>
                    <hr>
                    <div style="text-align:center">
                        <div class="row">
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Name</strong>
                                <br>
                                <p class="text-muted">{{@($data->name)?$data->name:''}}</p>
                            </div>
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Email</strong>
                                <br>
                                <p class="text-muted">{{@($data->email)?$data->email:'---'}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Mobile</strong>
                                <br>
                                <p class="text-muted">{{@($data->phone_no)?$data->phone_no:'---'}}</p>
                            </div>
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>User ID</strong>
                                <br>
                                <p class="text-muted">{{@($data->id)?$data->id:'---'}}</p>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Gender</strong>
                                <br>
                                <p class="text-muted">{{@($data->gender)?ucfirst($data->gender):'---'}}</p>
                            </div>
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>DOB</strong>
                                <br>
                                <p class="text-muted">{{@($data->dob)?date('d M,Y',strtotime($data->dob)):'---'}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>City</strong>
                                <br>
                                <p class="text-muted">{{@($data->city)?$data->city:'---'}}</p>
                            </div>
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Postal Code</strong>
                                <br>
                                <p class="text-muted">{{@($data->postal_code)?$data->postal_code:'---'}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Registration Type</strong>
                                <br>
                                <p class="text-muted">{{ ucfirst($data->register_with )}}</p>
                            </div>
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Social Type</strong>
                                <br>
                                <p class="text-muted">{{@($data->social_type)?$data->social_type:'---'}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Social Id</strong>
                                <br>
                                <p class="text-muted">{{@($data->social_id)?$data->social_id:'---'}}</p>
                            </div>
                            <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Status</strong>
                                <br>
                                @if($data->status==1)
                                <span class="text-success btn-sm">Active</span>
                                @else
                                <span class="text-danger btn-sm">Inactive</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a class="btn btn-success" href="javascript:void(0)" onclick="goBack()">Go Back</a>
                            </div>
                        </div>
                    </div>
                    <div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection