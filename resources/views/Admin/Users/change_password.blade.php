@extends('layouts.admin.admin')
@section('content')
    <div class="content">
        <div class="card">
            <div class="card-body">
                {!! Form::open(['route' => ['admin.change-password'], 'files' => 'true', 'id' => 'submit-form']) !!}
                @method('PATCH')
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="inputEmail">Old Password</label>
                            <?= Form::password('old_password', ['class' => 'form-control', 'placeholder' => 'Password', 'id' => 'old_password']) ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="inputEmail">Password</label>
                            <?= Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'id' => 'password']) ?>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="inputName">Confirm Password</label>
                            <?= Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password', 'id' => 'password_confirmation']) ?>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin: 1% 0;"></div>
                    <div class="col-sm-2">
                        {!! Form::button('<span id="licon"></span>Update', ['class' => 'btn btn-primary', 'id' => 'submit-btn', 'type' => 'submit']) !!}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#submit-form').ajaxForm({
                beforeSubmit: function() {
                    $(".error").remove();
                    disable("#submit-btn", true);
                },
                error: function(err) {
                    handleError(err);
                    disable("#submit-btn", false);
                },
                success: function(response) {
                    disable("#submit-btn", false);
                    if (response.status == 'true') {
                        window.location.href = '{{ route('admin.change-password') }}';
                    } else {
                        Alert(response.message, false);
                    }
                }
            });
        });
    </script>
@endsection
