@extends('layouts.admin.admin')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">

                <div class="form-group">
                    <label><strong>Status :</strong></label>
                    <select id='status' class="form-control" style="width: 200px">
                        <option value="">--Select Status--</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>

                </div>
                <div class="text-right mb-3">
                    <a href="{{ route('users.create') }}" class="btn btn-secondary">Add New</a>
                    <a class="btn btn-warning export" href="javascript:void(0)">Export to Excel</a>
                    <a class="btn btn-primary pdf" href="javascript:void(0)">Export to PDF</a>
                </div>
                <div class="row">
                    <div class="table-responsive">
                        <table id="users-table" class="display table table-striped table-hover w-100">
                            <thead>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Created on</th>
                                <th>Status</th>
                                <th width="100">Action</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    $(function() {
        var table = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('users.index') }}",
                data: function(d) {
                    d.status = $('#status').val(),
                        d.search = $('input[type="search"]').val()
                }
            },

            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'phone_no',
                    name: 'phone_no'
                },
                {
                    data: 'created_at',
                    name: 'created_at'
                },
                {
                    data: 'status',
                    name: 'status',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ],
            order: [
                [0, 'desc']
            ]
        });
        $('#status').change(function() {
            table.draw();
        });
    });

    // export user data
    $(document).on("click", ".export", function() {
        var status = $("#status").val();
        var route = "{{ route('admin.users.export') }}";
        route += '?status=' + status;
        window.location.href = route;
    });

    // export user data
    $(document).on("click", ".pdf", function() {
        var status = $("#status").val();
        var route = "{{ route('admin.users.export-pdf') }}";
        route += '?status=' + status;
        window.location.href = route;
    });
    
</script>
@endsection