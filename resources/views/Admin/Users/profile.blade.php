@extends('layouts.admin.admin')
@section('content')
    <div class="content">
        <div class="card">
            <div class="card-body">

                <div class="container-fluid">
                    @if (isset($user))
                        {{ Form::model($user, ['route' => ['admin.profile'], 'files' => 'true', 'id' => 'submit-form']) }}
                        @method('PATCH')
                    @endif
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputFullname">Name</label>
                                {!! Form::text('name', null, ['class' => 'form-control', 'maxlength' => '255', 'autofocus' => 'autofocus', 'placeholder' => 'Full Name', 'id' => 'name']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputEmail">Email address</label>
                                {!! Form::text('email', null, ['class' => 'form-control', 'maxlength' => '255', 'autofocus' => 'autofocus', 'placeholder' => 'Email', 'id' => 'email', 'required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label>Image</label>
                            @if (isset($user))
                                <input type="file" id="profile_picture"
                                    data-default-file="{{ $user->profile_picture && file_exists($user->profile_picture) ? url($user->profile_picture) : '' }}"
                                    name="profile_picture" class="dropify" data-height="150" data-show-remove="false"
                                    data-allowed-file-extensions="png jpeg jpg gif" />
                            @else
                                <input type="file" id="profile_picture" name="profile_picture" class="dropify"
                                    data-height="150" data-show-remove="false"
                                    data-allowed-file-extensions="png jpeg jpg gif" />
                            @endif

                        </div>


                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            {!! Form::button('<span id="licon"></span>Update', ['class' => 'btn btn-primary', 'id' => 'submit-btn', 'type' => 'submit']) !!}
                        </div>
                    </div>
                    <!-- .row -->
                    {{ Form::close() }}
                </div>
                <!-- .container-fluid -->
            </div>
            <!-- .card-body -->
        </div>
        <!-- .card -->

    </div>
@endsection
@section('scripts')
    <script>
        $(function() {
            $('#submit-form').ajaxForm({
                beforeSubmit: function() {
                    $(".error").remove();
                    disable("#submit-btn", true);
                },
                error: function(err) {
                    handleError(err);
                    disable("#submit-btn", false);
                },
                success: function(response) {
                    disable("#submit-btn", false);
                    if (response.status == 'true') {
                        window.location.href = '{{ route('admin.profile') }}';
                    } else {
                        Alert(response.message, false);
                    }
                }
            });
        });
    </script>
@endsection
