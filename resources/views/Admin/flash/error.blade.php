<!-- SweetAlert2 -->
<link rel="stylesheet" href="{{ url('public/AdminLTE/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{ url('public/AdminLTE/plugins/toastr/toastr.min.css') }}">
<!-- SweetAlert2 -->
<script src="{{ url('public/AdminLTE/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- Toastr -->
<script src="{{ url('public/AdminLTE/plugins/toastr/toastr.min.js') }}"></script>

<script type="text/javascript">
  $(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });
      Toast.fire({
        icon: 'error',
        title: "{{ Session::get('danger') }}"
      })
  });

</script>