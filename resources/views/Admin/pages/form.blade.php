<div class="form-group">
    <label>Page Tttle</label>
    {{ Form::text('title', null, ['placeholder' => 'Page title', 'id' => 'title', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    <label>Page Slug</label>
    {{ Form::text('slug', null, ['placeholder' => 'Page Slug', 'id' => 'slug', 'readonly' => 'readonly', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    <label>Description</label>
    {{ Form::textarea('description', null, ['placeholder' => 'description Slug', 'id' => 'description', 'class' => 'form-control']) }}
</div>

<script>
    $("#title").keyup(function() {
        var Text = $(this).val();
        Text = convertToName($.trim(Text));
        $("#slug").val(Text);
    });
    $("#title").change(function() {
        var Text = $(this).val();
        Text = convertToName($.trim(Text));
        $("#slug").val(Text);
    });
</script>
