@extends('layouts.admin.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {!! Form::model($data, ['route' => ['pages.update', [$id]], 'files' => 'true', 'class' => '', 'id' => 'submit-form', 'autocomplete' => 'off']) !!}
                    @csrf
                    @method('PATCH')
                    @include('Admin.pages.form');
                    {!! Form::button('<span id="licon"></span>Save', ['class' => 'btn btn-primary', 'id' => 'submit-btn', 'type' => 'submit']) !!}
                    <a class="btn btn-secondary" href="{{ route('pages.index') }}">Back</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#description').summernote({
            height: 300
        });

        $(function() {
            $('#submit-form').ajaxForm({
                beforeSubmit: function() {
                    $(".error").remove();
                    disable("#submit-btn", true);
                },
                error: function(err) {
                    handleError(err);
                    disable("#submit-btn", false);
                },
                success: function(response) {
                    disable("#submit-btn", false);
                    if (response.status == 'true') {
                        window.location.href = '{{ route('pages.index') }}';
                    } else {
                        Alert(response.message, false);
                    }
                }
            });
        });
    </script>
@endsection
