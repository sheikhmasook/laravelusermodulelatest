<div class=" p-4">
    <div class="form-row">
        <div class="form-group col-md-12">
            <label>Name</label>
            {{ Form::text('name', null, ['placeholder' => 'Name', 'id' => 'name', 'class' => 'form-control']) }}
        </div>

        <div class="form-group col-md-12">
            <label>Description</label>
            {{ Form::textArea('description', null, ['placeholder' => 'Description', 'id' => 'description', 'class' => 'form-control']) }}
        </div>


    </div>
</div>
