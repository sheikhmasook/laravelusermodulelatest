<?php

$saved_permissions = $entity->permissions;
$saved_permissions = $saved_permissions ? unserialize($saved_permissions) : [];
?>
@extends('layouts.admin.admin')
@section('content')
    {{ Form::model($entity, ['files' => 'true', 'class' => 'card', 'id' => 'submit-form', 'autocomplete' => 'off']) }}
    <div class=" p-4">
        <div class="form-row">
            <ul class="checktree">
                <?php foreach ($permissions as $m_key => $m_rows) {?>
                <li>
                    <?php if (is_array($m_rows)) {?>
                    <label><?= $m_key ?></label>
                    <ul>
                        <?php foreach ($m_rows as $c_key => $c_name) {?>
                        <li>
                            <?php $checked = in_array($c_key, $saved_permissions) ? 'checked' : null; ?>
                            <input name="permissions[<?= $c_key ?>]" id="<?= $c_key ?>" type="checkbox" value='1'
                                <?= $checked ?> /><label for="<?= $c_key ?>"><?= $c_name ?></label>

                        </li>
                        <?php }?>

                    </ul>
                    <?php } else {?>
                    <?php $checked = in_array($m_rows, $saved_permissions) ? 'checked' : null; ?>
                    <input name="permissions[<?= $m_rows ?>]" id="<?= $m_rows ?>" type="checkbox" value='1'
                        <?= $checked ?> /><label for="<?= $m_rows ?>"><?= $m_key ?></label>
                    <?php }?>
                </li>

                <?php }?>

            </ul>
        </div>
        <button class="btn btn-primary" id="submit-btn" type="submit"><span id="licon"></span>Save</button>
        <a class="btn btn-secondary" href="{{ route('roles.index') }}">Back</a>
    </div>
    {!! Form::close() !!}
@endsection

@section('styles')
    <style>
        ul {
            list-style-type: none;
            margin: 3px;
        }

        ul.checktree li:before {
            height: 1em;
            width: 12px;
            border-bottom: 1px dashed;
            content: "";
            display: inline-block;
            top: -0.3em;
        }

        ul.checktree li {
            border-left: 1px dashed;
        }

        ul.checktree li:last-child:before {
            border-left: 1px dashed;
        }

        ul.checktree li:last-child {
            border-left: none;
        }

    </style>
@endsection


@section('scripts')

    <script src="<?= asset('public/checktree/js/checktree.js') ?>"></script>

    <script>
        $(function() {
            $("ul.checktree").checktree();
        });
        $(function() {
            $('#submit-form').ajaxForm({
                beforeSubmit: function() {
                    $(".error").remove();
                    disable("#submit-btn", true);
                },
                error: function(err) {
                    handleError(err);
                    disable("#submit-btn", false);
                },
                success: function(response) {
                    disable("#submit-btn", false);
                    if (response.status == 'true') {
                        window.location.href = '{{ route('roles.index') }}';
                    } else {
                        Alert(response.message, false);
                    }
                }
            });
        });
    </script>
@endsection
