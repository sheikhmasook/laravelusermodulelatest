@extends('layouts.admin.admin')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="text-right mb-2">
                        <a href="{{ route('roles.create') }}" class="btn btn-secondary btn-md">Add New</a>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table id="users-table" class="display table table-striped table-hover w-100">
                                <thead>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th width="100">Action</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('roles.index') !!}',
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },

                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    }
                ],
                order: [
                    [0, 'desc']
                ]
            });
        })
    </script>
@endsection
