@php
$role_list = Config::get('params.role_list');
@endphp
@extends('layouts.admin.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(['route' => ['templates.store'], 'files' => 'true', 'class' => '', 'id' => 'submit-form', 'autocomplete' => 'off']) !!}
                    @method('POST')
                    @include('Admin.templates.form');
                    {!! Form::button('<span id="licon"></span>Save', ['class' => 'btn btn-primary', 'id' => 'submit-btn', 'type' => 'submit']) !!}
                    <a class="btn btn-secondary" href="{{ route('templates.index') }}">Back</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#content').summernote({
            height: 300
        });
        $("#title").keyup(function() {
            var Text = $(this).val();
            Text = convertToName($.trim(Text));
            $("#slug").val(Text);
        });
        $("#title").change(function() {
            var Text = $(this).val();
            Text = convertToName($.trim(Text));
            $("#slug").val(Text);
        });
        $(function() {
            $('#submit-form').ajaxForm({
                beforeSubmit: function() {
                    $(".error").remove();
                    disable("#submit-btn", true);
                },
                error: function(err) {
                    handleError(err);
                    disable("#submit-btn", false);
                },
                success: function(response) {
                    disable("#submit-btn", false);
                    if (response.status == 'true') {
                        window.location.href = '{{ route('templates.index') }}';
                    } else {
                        Alert(response.message, false);
                    }
                }
            });
        });
    </script>
@endsection
