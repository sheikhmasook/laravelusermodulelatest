<div class="form-group">
    <label>Title</label>
    {{ Form::text('title', null, ['placeholder' => 'Title', 'id' => 'title', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    <label>Field Name</label>
    {{ Form::text('slug', null, ['placeholder' => 'Slug Name', 'id' => 'slug', 'readonly' => 'readonly', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    <label>Keyword</label>
    {!! Form::text('keyword', null, ['placeholder' => 'Keyword', 'id' => 'keyword', 'class' => 'form-control ']) !!}
</div>

<div class="form-group">
    <label>Content</label>
    {!! Form::textarea('content', null, ['placeholder' => 'Enter Content Number', 'id' => 'content', 'class' => 'form-control']) !!}
</div>
