@extends('layouts.admin.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {!! Form::model($data, ['route' => ['settings.update', [$id]], 'files' => 'true', 'class' => '', 'id' => 'setting-form', 'autocomplete' => 'off']) !!}
                    @csrf
                    @method('PATCH')
                    @include('Admin.settings.form');
                    {!! Form::button('<span id="licon"></span>Update', ['class' => 'btn btn-primary', 'id' => 'submit-btn', 'type' => 'submit']) !!}
                    <a class="btn btn-secondary" href="{{ route('settings.index') }}">Back</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $("#field_title").keyup(function() {
            var Text = $(this).val();
            Text = convertToName($.trim(Text));
            $("#field_name").val(Text);
        });
        $("#field_title").change(function() {
            var Text = $(this).val();
            Text = convertToName($.trim(Text));
            $("#field_name").val(Text);
        });
        $(function() {
            $('#setting-form').ajaxForm({
                beforeSubmit: function() {
                    $(".error").remove();
                    disable("#submit-btn", true);
                },
                error: function(err) {
                    handleError(err);
                    disable("#submit-btn", false);
                },
                success: function(response) {
                    disable("#submit-btn", false);
                    if (response.status == 'true') {
                        window.location.href = '{{ route('settings.index') }}';
                    } else {
                        Alert(response.message, false);
                    }
                }
            });
        });

        $("#field_type").on('change', function(e) {
            var type = e.target.value;
            if (type == "image") {
                var append =
                    '<label>Field Value</label>{!! Form::file('value', ['id' => 'value', 'class' => 'form-control']) !!}<span class="text-danger value"></span>';
                $(".new-field").html(append);
                $("#value").dropify();
            } else {
                var append = '<label>Field Value</label><input type=' + type +
                    ' class="form-control" name="value" required="required" placeholder="Value"><span class="text-danger value"></span>';
                $(".new-field").html(append);
            }
        });
    </script>
@endsection
