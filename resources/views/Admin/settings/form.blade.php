<div class="form-group">
    <label>Field Title</label>
    {{ Form::text('field_title', null, ['placeholder' => 'Field Title', 'id' => 'field_title', 'class' => 'form-control']) }}
</div>
@if (isset($id))
    <div class="form-group">
        <label>Field Name</label>
        {{ Form::text('field_name', null, ['placeholder' => 'Field Name', 'readonly' => 'readonly', 'class' => 'form-control']) }}
        <span class="text-danger field_name"></span>
    </div>
@else
    <div class="form-group">
        <label>Field Name</label>
        {{ Form::text('field_name', null, ['placeholder' => 'Field Name', 'id' => 'field_name', 'readonly' => 'readonly', 'class' => 'form-control']) }}
        <span class="text-danger field_name"></span>
    </div>
@endif
@if (isset($id))
    <div class="form-group">
        <label>Field Type</label>
        {{ Form::select('field_type', ['text' => 'Text', 'image' => 'Image', 'email' => 'Email', 'number' => 'Number', 'url' => 'Url', 'date' => 'Date'], null, [
            'placeholder' => 'Select Field Type',
            'id' => 'field_type',
            'class' => 'form-control',
            'disabled' => true,
        ]) }}
        {{ Form::hidden('field_type', null) }}
    </div>
@else
    <div class="form-group">
        <label>Field Type</label>
        {{ Form::select('field_type', ['text' => 'Text', 'image' => 'Image', 'email' => 'Email', 'number' => 'Number', 'url' => 'Url', 'date' => 'Date'], null, [
            'placeholder' => 'Select Field Type',
            'id' => 'field_type',
            'class' => 'form-control',
        ]) }}
    </div>
@endif
<div class="form-group new-field">
    @if (isset($id))
        @if ($data->field_type == 'image')
            <label>Field Value</label>
            <input type="file" id="value"
                data-default-file="{{ $data->value && file_exists($data->value) ? url($data->value) : '' }}"
                name="value" class="dropify" data-height="150" data-show-remove="false"
                data-allowed-file-extensions="png jpeg jpg gif" />
        @else
            <label>Field Value</label>
            {!! Form::text('value', null, ['placeholder' => 'Value', 'id' => 'value', 'class' => 'form-control']) !!}
        @endif
    @endif
</div>
