<div class=" p-4">
    <div class="form-row">

        <div class="form-group col-md-6">
            <label for="name">Name </label>
            <?= Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control', 'required' => true, 'id' => 'first_name']) ?>
        </div>

        <div class="form-group col-md-6">
            <label for="email">Email Address</label>
            <?= Form::email('email', null, ['placeholder' => 'Email Address', 'class' => 'form-control', 'required' => true, 'id' => 'email']) ?>
        </div>

        <div class="form-group col-md-6">
            <label for="phone_number">Phone Number</label>
            <?= Form::text('phone_number', null, ['placeholder' => 'Phone Number', 'class' => 'form-control', 'id' => 'phone_number']) ?>
        </div>

        <div class="form-group col-md-6">
            <label for="role_id">Role</label>
            <?= Form::select('role_id', $roles, null, ['placeholder' => 'Choose...', 'class' => 'form-control', 'required' => true, 'id' => 'role_id']) ?>
        </div>



    </div>
</div>
