@php
$action = app('request')
    ->route()
    ->getAction();
$controller = class_basename($action['controller']);
[$controller, $action] = explode('@', $controller);
@endphp


<div class="sidebar sidebar-style-2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            @include('Admin.includes.header')
            <ul class="nav nav-primary">
                <li class="nav-item {{ request()->is('admin/dashboard') ? 'active submenu' : '' }}">
                    <a href="{{ route('admin.dashboard') }}">
                        <i class="fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <!-- users Management -->
                @if ($role_permissions == -1 || in_array('UsersController', $role_permissions))
                    <li
                        class="nav-item {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active submenu' : '' }}">
                        <a data-toggle="collapse" href="#users">
                            <i class="fas fa-user"></i>
                            <p>Users Management</p>
                            <span class="caret"></span>
                        </a>

                        <div class="collapse {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'show' : '' }}"
                            id="users">
                            <ul class="nav nav-collapse">
                                <li
                                    class="{{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                    <a href="{{ route('users.index') }}">
                                        <span class="sub-item">List of Users</span>
                                    </a>
                                </li>

                              
                            </ul>
                        </div>
                    </li>
                @endif
                <!-- role Management -->
                @if ($role_permissions == -1 || in_array('AdminsController', $role_permissions))
                    <li
                        class="nav-item {{ request()->is('admin/roles') || request()->is('admin/roles/*') || request()->is('admin/admins') || request()->is('admin/admins/*') ? 'active submenu' : '' }}">
                        <a data-toggle="collapse" href="#admins">
                            <i class="fas fa-tasks"></i>
                            <p>Role Management</p>
                            <span class="caret"></span>
                        </a>

                        <div class="collapse {{ request()->is('admin/admins') || request()->is('admin/admins/*') || request()->is('admin/roles') || request()->is('admin/roles/*') ? 'show' : '' }}"
                            id="admins">
                            <ul class="nav nav-collapse">

                                <li
                                    class="{{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                    <a href="{{ route('roles.index') }}">
                                        <span class="sub-item">List of roles</span>
                                    </a>
                                </li>

                                <li
                                    class="{{ request()->is('admin/admins') || request()->is('admin/admins/*') ? 'active' : '' }}">
                                    <a href="{{ route('admins.index') }}">
                                        <span class="sub-item">List of Backend users</span>
                                    </a>
                                </li>



                            </ul>
                        </div>
                    </li>
                @endif
                {{-- template management --}}
                @if ($role_permissions == -1 || in_array('TemplatesController', $role_permissions))
                    <li
                        class="nav-item {{ request()->is('admin/templates') || request()->is('admin/templates/*') ? 'active submenu' : '' }}">
                        <a data-toggle="collapse" href="#templates">
                            <i class="fas fa-envelope"></i>
                            <p>Templates Management</p>
                            <span class="caret"></span>
                        </a>

                        <div class="collapse {{ request()->is('admin/templates') || request()->is('admin/templates/*') ? 'show' : '' }}"
                            id="templates">
                            <ul class="nav nav-collapse">

                                <li
                                    class="{{ request()->is('admin/templates') || request()->is('admin/templates/*') ? 'active' : '' }}">
                                    <a href="{{ route('templates.index') }}">
                                        <span class="sub-item">List of Templates</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endif
                {{-- cms management --}}
                @if ($role_permissions == -1 || in_array('CmsController', $role_permissions))
                    <li
                        class="nav-item {{ request()->is('admin/pages') || request()->is('admin/pages/*') ? 'active submenu' : '' }}">
                        <a data-toggle="collapse" href="#cms">
                            <i class="fas fa-file-alt"></i>
                            <p>CMS Management</p>
                            <span class="caret"></span>
                        </a>

                        <div class="collapse {{ request()->is('admin/pages') || request()->is('admin/pages/*') ? 'show' : '' }}"
                            id="cms">
                            <ul class="nav nav-collapse">
                                <li
                                    class="{{ request()->is('admin/pages') || request()->is('admin/pages/*') ? 'active' : '' }}">
                                    <a href="{{ route('pages.index') }}">
                                        <span class="sub-item">List of Pages</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endif
                <!-- setting management -->
                @if ($role_permissions == -1 || in_array('SettingController', $role_permissions))

                    <li
                        class="nav-item {{ request()->is('admin/settings') || request()->is('admin/settings/*') ? 'active submenu' : '' }}">
                        <a data-toggle="collapse" href="#setting">
                            <i class="fas fa-cog"></i>
                            <p>Setting Management</p>
                            <span class="caret"></span>
                        </a>

                        <div class="collapse {{ request()->is('admin/settings') || request()->is('admin/settings/*') ? 'show' : '' }}"
                            id="setting">
                            <ul class="nav nav-collapse">

                                <li class="{{ request()->is('admin/settings/*') ? 'active' : '' }}">
                                    <a href="{{ route('settings.index') }}">
                                        <span class="sub-item">Site Setting</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>
