<div class="user">
    @if (isset(Auth::guard('admin')->user()->profile_picture) && file_exists(Auth::guard('admin')->user()->profile_picture))
        <div class="avatar-sm float-left mr-2">
            <img src="{{ url(Auth::guard('admin')->user()->profile_picture) }}" alt="..."
                class="avatar-img rounded-circle">
        </div>
    @else
        <div class="avatar-sm float-left mr-2">
            <img src="{{ url('public/admin/img/default-user.png') }}" alt="..." class="avatar-img rounded-circle">
        </div>
    @endif
    <div class="info">
        <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
            <span>
                {{ Auth::guard('admin')->user()->name }}
                <span class="user-level">Administrator</span>
                <span class="caret"></span>
            </span>
        </a>
        <div class="clearfix"></div>
        <div class="collapse in" id="collapseExample">
            <ul class="nav">
                <li>
                    <a href="{{ route('admin.profile') }}">
                        <span class="link-collapse">My Profile</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.logout') }}">
                        <span class="link-collapse">Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
