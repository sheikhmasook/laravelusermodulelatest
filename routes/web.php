<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Auth::routes();
// Route::get('send-mail', [MailController::class, 'sendMail'])->name('sendMail');

Route::group(['namespace' => 'Front'], function () {
	Route::get('/', 'HomeController@index')->name('front.home.index');
	Route::get('/page/{slug}', 'PagesController@AppPages');
});


