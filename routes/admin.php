<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MailController;
use App\Http\Controllers\Admin\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Admin'], function () {
	Route::any('/', [LoginController::class, 'login']);
	Route::any('/login', [LoginController::class, 'login'])->name('admin.login');
	Route::middleware(['admin', 'CheckPermission'])->group(static function () {
		//::users route
		Route::any('user/delete', 'UsersController@delete')->name('users.delete');
		Route::any('dashboard', 'UsersController@dashboard')->name('admin.dashboard');
		Route::any('user/profile', 'UsersController@profile')->name('admin.profile');
		Route::any('user/change-password', 'UsersController@changePassword')->name('admin.change-password');
		Route::any('user/status', 'UsersController@status')->name('users.status');
		Route::any('app-setting', 'UsersController@appSetting')->name('admin.app-setting');
		Route::any('export', 'UsersController@export')->name('admin.users.export');
		Route::any('pdf', 'UsersController@createPDF')->name('admin.users.export-pdf');
		Route::resource('users', UsersController::class);

		//  admins route
		Route::post('admins/delete', 'AdminsController@delete')->name('admins.delete');
		Route::post('admins/status', 'AdminsController@status')->name('admins.status');
		Route::resource('admins', AdminsController::class);

		//  roles route
		Route::post('roles/delete', 'RolesController@delete')->name('roles.delete');
		Route::post('roles/status', 'RolesController@status')->name('roles.status');
		Route::any('roles/permissions/{id?}', 'RolesController@permissions')->name('roles.permissions');
		Route::resource('roles', RolesController::class);

		//  template route
		Route::post('template/status', 'TemplatesController@status')->name('templates.status');
		Route::resource('templates', TemplatesController::class);

		// page routes
		Route::post('pages/status', 'CmsController@status')->name('pages.status');
		Route::resource('pages', CmsController::class);

		//::site setting
		Route::resource('settings', SettingsController::class);
		Route::any('/logout', [LoginController::class, 'logout'])->name('admin.logout');
	});
});
