<?php
return [
	'role_list' => ['admin' => 'Admin', 'user' => 'User', 'subadmin' => 'Subadmin'],
	'permissions' => [
		'Manage User' => [
			'UsersController@index' => 'View Users',
			'UsersController@delete' => 'Delete Users',
		],
		'Roles' => [
			'RolesController' => 'Role Management',
			'AdminsController' => 'Backend Users Management',
		],

		'Template Management' => 'TemplatesController',
		'CMS Management' => 'CmsController',


	],
];
