<?php

use App\Scopes\HasActiveScope;

trait ActiveScope 
{
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    public static function bootSoftDeletes()
{
    static::addGlobalScope(new HasActiveScope);
}
}