<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;
use App\Mail\NotifyMail;
use App\Models\User;
use App\Models\Template;

class EmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $mail_data = Template::where('slug', '=', 'registration-mail')->first();
        User::orderBy('id')->chunk(100, function ($users, $mail_data) {
            $site_email      = env('MAIL_FROM_ADDRESS');
            $site_title      = env('APP_NAME');
            $subject         = $mail_data->subject;
            foreach ($users as $key => $user) {
                $data['user_name'] = $user->name;
                $email = $user->email;
                $message         = str_replace(explode(",", $mail_data->keyword), $data, $mail_data->content);

                Mail::send('email.template', array('data' => $message), function ($message) use ($site_email, $email, $subject, $site_title) {
                    $message->from($site_email, $site_title)
                        ->to($email, $email)
                        ->subject($subject);
                });
            }
        });
    }
}
