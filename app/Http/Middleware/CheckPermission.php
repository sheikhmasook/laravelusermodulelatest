<?php

namespace App\Http\Middleware;

use App\Lib\Helper;
use Auth;
use Closure;
use Config;
use Illuminate\Support\Facades\View;

class CheckPermission
{

    public function handle($request, Closure $next)
    {
        $action = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);
        list($controller, $action) = explode('@', $controller);
        //$p = $request->route()->parameter('id');
        $cont_action = $controller . '@' . $action;
        $user = Auth::guard('admin')->user();
        $role_id = $user->role_id;
        //$role_id = 1;
        if ($role_id == -1) {
            $role_permissions = -1;
        } else {
            $role_permissions = Helper::getRolePermissions($role_id);
        }
        $permssions = Config::get('params.permissions');
        $cont_exists = in_array($controller, $permssions);
        $cont_action_exists = Helper::multiKeyExists($permssions, $cont_action);
        if ($role_permissions != -1 && ($cont_action_exists || $cont_exists) && (!in_array($cont_action, $role_permissions) && !in_array($controller, $role_permissions))) {
            abort(403, 'You are not authorized to access this page.');
        }
        \Config::set('role_permissions', $role_permissions);
        View::share('role_permissions', $role_permissions);
        return $next($request);
    }
}
