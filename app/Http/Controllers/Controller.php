<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    function validationHandle($validation) {
		foreach ($validation->getMessages() as $field_name => $messages) {
			if (!isset($firstError)) {
				$firstError = $messages[0];
				$error[$field_name] = $messages[0];
			}
		}
		return $firstError;
	}
	function response($data) {
		$data = $this->arrayHandleFun($data);
		// This header pass for allow origin control.
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Expose-Headers: Content-Length, X-JSON");
		header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
		header("Access-Control-Allow-Headers: *");
		echo json_encode($data);die();
	}
	function arrayHandleFun($array, $isRepeat = false) {

		foreach ($array as $key => $value) {
			if ($value === null) {
				$array[$key] = "";
			} else if (is_array($value)) {
				if (empty($value)) {
					//$array[$key] = "";
				} else {
					$array[$key] = SELF::arrayHandleFun($value);
				}
			}
		}
		if (!$isRepeat) {
			$array = SELF::arrayHandleFun($array, true);
		}
		return $array;
	}

	function randomPassword() {
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}

}
