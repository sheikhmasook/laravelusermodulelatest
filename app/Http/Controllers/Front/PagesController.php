<?php
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Page;
use App\Models\User;
use Auth;

class PagesController extends Controller {

	public function showPages($slug) {
		if ($slug == 'faq') {
			$row = Faq::where(['status' => 1])->get();
		} else {
			$row = Page::where(['slug' => $slug, 'status' => 1])->first();
		}
		if ($row) {
			return view('Front.pages.' . $slug, compact('row'));
		}
	}
	public function AppPages($slug) {
		if ($slug == 'faq') {
			$row = Faq::where(['status' => 1])->get();
		} else {
			$row = Page::where(['slug' => $slug, 'status' => 1])->first();
		}
		if ($row) {
			// echo '<pre>';
			// print_r($row);die;
			return view('Front.pages.' . 'app-' . $slug, compact('row'));
		}
	}

	

}
