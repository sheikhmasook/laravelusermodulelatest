<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Hash;



class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin\dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.guest')->except('logout');
    }


    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required',
                ]);

                if ($validator->passes()) {
                    $admin = Admin::with(['role'])->where('email', $request->get('email'))->first();
                    if ($admin) {
                        if ($admin->status == 1) {
                            if (Hash::check($request->get('password'), $admin->password)) {
                                if (Auth::guard('admin')->attempt(['email' => $request->get('email'), 'password' => $request->get('password'), 'status' => 1], true)) {
                                    if (session()->has('url.intended')) {
                                        return redirect()->intended();
                                    }
                                    return redirect()->route('admin.dashboard');
                                } else {
                                    Session::flash('danger', 'Something went wrong, please try again.');
                                    return redirect()->back()->withInput()->withErrors($validator->errors());
                                }
                            } else {
                                Session::flash('danger', 'Invalid email or password.');
                                return redirect()->back()->withInput()->withErrors($validator->errors());
                            }
                        } else {
                            Session::flash('danger', 'You have marked inactive by admin.');
                            return redirect()->back()->withInput()->withErrors($validator->errors());
                        }
                    } else {
                        Session::flash('danger', 'Invalid email or password.');
                        return redirect()->back()->withInput()->withErrors($validator->errors());
                    }
                } else {
                    Session::flash('danger', $this->validationHandle($validator->errors()));
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                Session::flash('danger', $msg);
                return redirect()->back()->withInput();
            }
        }
        return view('Admin.auth.login');
    }
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login')->with('success', 'Logout Successfully!');
    }
}
