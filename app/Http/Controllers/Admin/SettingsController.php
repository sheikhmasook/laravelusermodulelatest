<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;
use Session;
use Illuminate\Support\Facades\DB;
use App\Lib\Uploader;
use App\Lib\Helper;
use Illuminate\Support\Facades\Crypt;
use DataTables;
use Cache;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "Settings";
        $breadcrumbs = [
            ['name' => 'Settings', 'relation' => 'Current', 'url' => '']
        ];
        if ($request->ajax()) {
            $settings = Setting::select(['id', 'field_title', 'field_name', 'field_type', 'value'])->get();
            return DataTables::of($settings)
                ->addColumn('action', function ($setting) {
                    $setting_id = Crypt::encrypt($setting->id);
                    return '<a href="' . route('settings.edit', $setting_id) . '" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>';
                })
                ->editColumn('value', function ($setting) {
                    if ($setting->field_type == 'image') {
                        return Helper::getImage($setting->value, '60', '80');
                    } else {
                        return $setting->value;
                    }
                })
                ->rawColumns(['value', 'action'])
                ->make(true);
        }
        return view('Admin.settings.index', compact('title', 'breadcrumbs'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $title = "Add Setting";
            $breadcrumbs = [
                ['name' => 'Settings', 'relation' => 'link', 'url' => route('settings.index')],
                ['name' => 'Add Setting', 'relation' => 'Current', 'url' => '']
            ];
            return view('Admin.settings.create', compact('title', 'breadcrumbs'));
        } catch (\Throwable $th) {
            return ['status' => 'false', 'message' => $th->getMessage()];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        DB::beginTransaction();
        try {
            $validator = Setting::validate($request->all(), '');
            if ($validator->fails()) {
                return response()->json(array('errors' => $validator->messages()), 422);
            } else {
                $formData = $request->except('value');
                if ($request->field_type == "image") {
                    if ($request->hasFile('value')) {
                        if (substr($request->file('value')->getMimeType(), 0, 5) == 'image') {
                            $path = "/uploads/settings/";
                            $responseData =  Uploader::doUpload($request->file('value'), $path);
                            $formData['value'] = $responseData['file'];
                        } else {
                            return ['status' => 'false', 'message' => 'The file must be an image'];
                            exit();
                        }
                    }
                } else {
                    $formData['value'] = $request->get('value');
                }

                Setting::create($formData);
                DB::commit();
                Session::flash('success', 'Setting created successfully');
                Cache::flush();
                return ['status' => 'true', 'message' => 'Records updated successfully'];
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 'false', 'message' => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decrypt($id);
            $data = Setting::findorfail($id);
            $title = "Edit Setting";
            $breadcrumbs = [
                ['name' => 'Settings', 'relation' => 'link', 'url' => route('settings.index')],
                ['name' => 'Edit Setting', 'relation' => 'Current', 'url' => '']
            ];
            return view('Admin.settings.edit', compact('title', 'breadcrumbs', 'data', 'id'));
        } catch (\Throwable $th) {
            return ['status' => 'false', 'message' => $th->getMessage()];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Setting::validate($request->all(), $id);
            if ($validator->fails()) {
                return response()->json(array('errors' => $validator->messages()), 422);
            } else {
                $data = ($id) ? Setting::findorfail($id) : array();
                $formData = $request->except('value');
                if ($request->field_type == "image") {
                    if ($request->hasFile('value')) {
                        if (substr($request->file('value')->getMimeType(), 0, 5) == 'image') {
                            $path = "/uploads/settings/";
                            $responseData =  Uploader::doUpload($request->file('value'), $path);
                            $formData['value'] = $responseData['file'];
                        } else {
                            return ['status' => 'false', 'message' => 'The file must be an image'];
                            exit();
                        }
                    }
                } else {
                    $formData['value'] = $request->get('value');
                }

                $data->update($formData);
                DB::commit();
                Session::flash('success', 'Setting updated successfully');
                Cache::flush();
                return ['status' => 'true', 'message' => 'Records updated successfully'];
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 'false', 'message' => $e->getMessage()];
        }
    }
}
