<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Models\User;
use App\Models\Admin;
use App\Models\AppSetting;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use DataTables;
use App\Lib\Helper;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Excel;
use App\Exports\ExportData;
use PDF;

class UsersController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $breadcrumbs = [
      ['name' => 'Users', 'relation' => 'Current', 'url' => '']
    ];
    $page_title = 'Users List';
    if ($request->ajax()) {
      $users = User::orderBy('id', 'desc');
      return DataTables::of($users)
        ->addColumn('action', function ($user) {
          $user_id = Crypt::encrypt($user->id);
          $btn = '<a title="Edit" href="' . route('users.edit', $user_id) . '" class="btn btn-xs btn-info"><i class="fas fa-edit"></i></a>&nbsp;';
          $btn .= '<a title="View" href="' . route('users.show', $user_id) . '" class="btn btn-xs btn-primary"><i class="fas fa-eye"></i></a>&nbsp;';
          $btn .= '<a title="Delete" data-link="' . route('users.delete') . '" id="delete_' . $user->id . '" onclick="confirm_delete(' . $user->id . ')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i></a>&nbsp;';
          return $btn;
        })
        ->editColumn('status', function ($user) {
          return Helper::getStatus($user->status, $user->id, route('users.status'));
        })
        ->editColumn('created_at', function ($user) {
          return getformattedDate($user->created_at);
        })
        ->filter(function ($instance) use ($request) {
          if ($request->get('status') == '0' || $request->get('status') == '1') {
            $instance->where('status', $request->get('status'));
          }
          if (!empty($request->get('search'))) {
            $instance->where(function ($w) use ($request) {
              $search = $request->get('search');
              $w->orWhere('first_name', 'LIKE', "%$search%")
                ->orWhere('email', 'LIKE', "%$search%");
            });
          }
        })
        ->rawColumns(['status', 'action'])
        ->make(true);
    }
    return view('Admin.Users.index', compact('page_title', 'breadcrumbs'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    try {
      $page_title = 'Add User';
      $breadcrumbs = [
        ['name' => 'Users', 'relation' => 'link', 'url' => route('users.index')],
        ['name' => 'Add User', 'relation' => 'Current', 'url' => '']
      ];
      return view('Admin.Users.create', compact('page_title', 'breadcrumbs'));
    } catch (\Throwable $th) {
      return ['status' => 'false', 'message' => $th->getMessage()];
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    DB::beginTransaction();
    try {
      if ($request->isMethod('post')) {
        $postdata  = $request->all();
        $rules = [
          'first_name' => 'required|max:50',
          'last_name' => 'required|max:50',
          'phone_no' => 'required|unique:users,phone_no',
          'email' => 'required|email|unique:users,email',
          'description' => 'required',
          'profile_picture' => 'nullable'
        ];

        $validator = Validator::make($postdata, $rules);
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->errors()), 422);
        } else {
          $postdata['register_with'] = 'ADMIN';
          if ($request->hasFile('profile_picture')) {
            $img = $request->file('profile_picture');
            $file_ext = $img->getClientOriginalExtension();
            $filename = time() . '.' . $file_ext;
            $destination_path = 'uploads/users/' . $filename;
            if (Storage::disk('public')->put($destination_path, file_get_contents($img->getRealPath()))) {
              $postdata['profile_picture'] = 'public/' . $destination_path;
            }
          }
          User::create($postdata);
          DB::commit();
          Session::flash('success', 'User Create Successfully');
          return ['status' => 'true', 'message' => 'User Create Successfully.'];
        }
      }
    } catch (\Throwable $th) {
      DB::rollBack();
      return ['status' => 'false', 'message' => $th->getMessage()];
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $id = Crypt::decrypt($id);
    $page_title = 'View User';
    $breadcrumbs = [
      ['name' => 'Users', 'relation' => 'link', 'url' => route('users.index')],
      ['name' => 'View User', 'relation' => 'Current', 'url' => '']
    ];
    $data = User::where('id', $id)->first();
    if ($data) {
      $page_title = "Profile - " . $data->name;
      return view('Admin.Users.view', compact('page_title', 'data', 'breadcrumbs'));
    } else {
      return abort(404);
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    try {
      $id = Crypt::decrypt($id);
      $getuser = User::findOrFail($id);
      $page_title = 'Edit User';
      $breadcrumbs = [
        ['name' => 'Users', 'relation' => 'link', 'url' => route('users.index')],
        ['name' => 'Edit User', 'relation' => 'Current', 'url' => '']
      ];
      return view('Admin.Users.edit', compact('page_title', 'breadcrumbs', 'getuser', 'id'));
    } catch (\Throwable $th) {
      return ['status' => 'false', 'message' => $th->getMessage()];
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    DB::beginTransaction();
    try {
      $getuser = User::findOrFail($id);
      $postdata  = $request->all();
      $rules = [
        'first_name' => 'required|max:50',
        'last_name' => 'required|max:50',
        'phone_no' => 'required|unique:users,phone_no,' . $id,
        'email' => 'required|email|unique:users,email,' . $id,
        'description' => 'required',
        'profile_picture' => 'nullable'
      ];
      $validator = Validator::make($postdata, $rules);
      if ($validator->fails()) {
        return response()->json(array('errors' => $validator->errors()), 422);
      } else {

        if ($request->hasFile('profile_picture')) {
          $img = $request->file('profile_picture');
          $file_ext = $img->getClientOriginalExtension();
          $filename = time() . '.' . $file_ext;
          $destination_path = 'uploads/users/' . $filename;
          if (Storage::disk('public')->put($destination_path, file_get_contents($img->getRealPath()))) {
            $postdata['profile_picture'] = 'public/' . $destination_path;
          }
        } else {
          $postdata['profile_picture'] = $getuser->profile_picture;
        }
        $getuser->update($postdata);
        DB::commit();
        Session::flash('success', 'User update Successfully');
        return ['status' => 'true', 'message' => 'User update Successfully.'];
      }
    } catch (\Throwable $th) {
      DB::rollBack();
      return ['status' => 'false', 'message' => $th->getMessage()];
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function delete(Request $request)
  {
    try {
      $data = User::where('id', $request->id)->first();
      File::delete($data->profile_picture);
      if ($data->delete()) {
        return ['status' => 'true', 'message' => 'User delete Successfully'];
      } else {
        return abort(404);
      }
    } catch (\Throwable $th) {
      return ['status' => 'false', 'message' => $th->getMessage()];
    }
  }



  public function dashboard()
  {
    try {
      $page_title = 'Dashboard';
      // total users
      $total_users = User::where(['role' => 'user'])->count();
      // active users
      $active_users = User::where(['status' => '1', 'role' => 'user'])->count();
      // new users
      $new_users = User::where(['status' => '1', 'role' => 'user', 'created_at' => date('Y-m-d')])->count();
      return view('Admin.Users.dashboard', compact('total_users', 'page_title', 'active_users', 'new_users'));
    } catch (\Throwable $th) {
      return ['status' => 'false', 'message' => $th->getMessage()];
    }
  }
  public function profile(Request $request)
  {
    DB::beginTransaction();
    try {
      $page_title = 'Admin Profile';
      $id = Auth::guard('admin')->user()->id;
      $user = Admin::findorFail($id);
      $postdata = $request->all();

      if ($request->isMethod('patch')) {
        $rules = [
          'name' => 'required',
          'email' => 'required|email|unique:users,email,' . $id,
          'profile_picture' => 'nullable',
        ];

        $validator = Validator::make($postdata, $rules);
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->errors()), 422);
        } else {

          if ($request->hasFile('profile_picture')) {
            $img = $request->file('profile_picture');
            $file_ext = $img->getClientOriginalExtension();
            $filename = time() . '.' . $file_ext;
            $destination_path = 'uploads/users/' . $filename;
            if (Storage::disk('public')->put($destination_path, file_get_contents($img->getRealPath()))) {
              $postdata['profile_picture'] = 'public/' . $destination_path;
            }
          } else {
            $postdata['profile_picture'] = $user->profile_picture;
          }
          $user->update($postdata);
          DB::commit();
          Session::flash('success', 'Profile updated Successfully');
          return ['status' => 'true', 'message' => 'Profile updated Successfull'];
        }
      }
      return view('Admin.Users.profile', compact('page_title', 'user'));
    } catch (\Throwable $th) {
      DB::rollBack();
      return ['status' => 'false', 'message' => $th->getMessage()];
    }
  }


  public function status(Request $request)
  {
    DB::beginTransaction();
    try {
      $id = $request->id;
      $row = User::whereId($id)->first();
      $row->status = $row->status == '1' ? '0' : '1';
      $row->save();
      DB::commit();
      return Helper::getStatus($row->status, $id, route('users.status'));
    } catch (\Throwable $th) {
      DB::rollBack();
      return ['status' => 'false', 'message' => $th->getMessage()];
    }
  }

  public function appSetting(Request $request)
  {
    $title = "App Setting";
    $breadcrumbs = [
      ['name' => 'App Setting', 'relation' => 'Current', 'url' => ''],
    ];
    $data = AppSetting::where('id', '1')->first();
    if ($request->ajax() && $request->isMethod('post')) {
      DB::beginTransaction();
      try {
        $validator = Validator::make($request->all(), [
          'android_version' => 'required|numeric',
          'ios_version' => 'required|numeric',
        ]);
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->errors()), 422);
        } else {
          $formData = $request->except('_token');
          AppSetting::where('id', '1')->update($formData);
          DB::commit();
          return ['status' => 'true', 'message' => 'App Setting updated successfully'];
        }
      } catch (\Exception $e) {
        DB::rollBack();
        return ['status' => 'false', 'message' => $e->getMessage()];
      }
    }
    return view('Admin.Users.app-setting', compact('data', 'title', 'breadcrumbs'));
  }


  public function changePassword(Request $request)
  {
    DB::beginTransaction();
    try {
      $page_title = 'Change Password';
      $id = Auth::guard('admin')->user()->id;
      $user = User::findorFail($id);
      if ($request->isMethod('patch')) {
        $postdata  = $request->all();
        $validator = Validator::make($postdata, [
          'old_password' => 'required',
          'password' => 'required|min:8',
          'password_confirmation' => 'required|same:password|min:6',
        ]);
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->errors()), 422);
        } else {
          if (Hash::check($postdata['old_password'], $user->password)) {
            $user->password = Hash::make($postdata['password']);
            $user->update($postdata);
            DB::commit();
            Session::flash('success', 'Password Change Successfully');
            return ['status' => 'true', 'message' => 'Password Change Successfully'];
          }
        }
      }
      return view('Admin.Users.change_password', compact('user', 'page_title'));
    } catch (\Throwable $th) {
      DB::rollBack();
      return ['status' => 'false', 'message' => $th->getMessage()];
    }
  }

  //:: export user data

  public function export(Request $request)
  {
    try {
      $exportData = User::orderBy('id', 'desc')
        ->when(!empty($request->status), function ($query) use ($request) {
          $query->where('status', $request->status);
        })
        ->get();
      $file_name = "user_" . date('Y-m-d H:i') . ".xlsx";
      return Excel::download(new ExportData($exportData, 'Users'), $file_name);
    } catch (\Throwable $th) {
      return ['status' => 'false', 'message' => $th->getMessage()];
    }
  }

  // Generate PDF
  public function createPDF(Request $request)
  {
    try {
      // retreive all records from db
      $data = User::orderBy('id', 'desc')
        ->when(!empty($request->status), function ($query) use ($request) {
          $query->where('status', $request->status);
        })
        ->get();
      $pdf = PDF::setOptions(['isHtml5ParsedEnabled' => true])->loadView('Admin.Users.pdf', compact('data'));
      // download PDF file with download method
      $file_name = "user_" . date('Y-m-d H:i') . ".pdf";
      return $pdf->download($file_name);
    } catch (\Throwable $th) {
      return ['status' => 'false', 'message' => $th->getMessage()];
    }
  }
}
