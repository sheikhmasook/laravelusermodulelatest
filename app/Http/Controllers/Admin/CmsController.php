<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Lib\Helper;
use App\Models\Page;
use DataTables;
use Session;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "Pages";
        $breadcrumbs = [
            ['name' => 'Pages', 'relation' => 'Current', 'url' => ''],
        ];
        if ($request->ajax()) {
            $pages = Page::where('status', 1)->select(['id', 'title', 'slug', 'status']);

            return DataTables::of($pages)
                ->addColumn('action', function ($page) {
                    $page_id = Crypt::encrypt($page->id);

                    $btn = '<a href="' . route('pages.edit', $page_id) . '" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>&nbsp;';
                    $btn .= '<a href="' . route('pages.show', $page_id) . '" class="btn btn-xs btn-primary"><i class="fas fa-eye"></i> View</a>&nbsp;';
                    return $btn;
                })
                ->editColumn('status', function ($page) {
                    return Helper::getStatus($page->status, $page->id, route('pages.status'));
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
        }
        return view('Admin.pages.index', compact('title', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $title = "Add New page";
            $breadcrumbs = [
                ['name' => 'Pages', 'relation' => 'link', 'url' => route('pages.index')],
                ['name' => 'Add New Page', 'relation' => 'Current', 'url' => ''],
            ];
            return view('Admin.pages.create', compact('title', 'breadcrumbs'));
        } catch (\Throwable $th) {
            return ['status' => 'false', 'message' => $e->getMessage()];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'slug' => 'required|max:255|unique:pages,slug',
                'description' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(array('errors' => $validator->messages()), 422);
            } else {
                $formData = $request->all();
                Page::create($formData);
                DB::commit();
                Session::flash('success', 'Page created successfully');
                return ['status' => 'true', 'message' => 'Records created successfully'];
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 'false', 'message' => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function status(Request $request)
    {
        DB::beginTransaction();
        try {
            $id = $request->id;
            $row = Page::whereId($id)->first();
            $row->status = $row->status == '1' ? '0' : '1';
            $row->save();
            DB::commit();
            return Helper::getStatus($row->status, $id, route('pages.status'));
        } catch (\Throwable $th) {
            DB::rollBack();
            return ['status' => 'false', 'message' => $e->getMessage()];
        }
    }


    public function show($id)
    {
        try {
            $id = Crypt::decrypt($id);
            $title = "Page Detail";
            $breadcrumbs = [
                ['name' => 'Page', 'relation' => 'link', 'url' => route('pages.index')],
                ['name' => $title, 'relation' => 'Current', 'url' => ''],
            ];
            $data = Page::where('id', $id)->where('status', 1)->first();
            return view('Admin.pages.view', compact('data', 'title', 'breadcrumbs'));
        } catch (\Throwable $th) {
            return ['status' => 'false', 'message' => $th->getMessage()];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decrypt($id);
            $data = Page::findorFail($id);
            $title = "Edit page";
            $breadcrumbs = [
                ['name' => 'Pages', 'relation' => 'link', 'url' => route('pages.index')],
                ['name' => 'Edit Page', 'relation' => 'Current', 'url' => ''],
            ];
            return view('Admin.pages.edit', compact('title', 'breadcrumbs', 'data', 'id'));
        } catch (\Throwable $th) {
            return ['status' => 'false', 'message' => $th->getMessage()];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'slug' => 'required|max:255|unique:pages,slug,' . $id,
                'description' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(array('errors' => $validator->messages()), 422);
            } else {
                $data = Page::findorFail($id);
                $formData = $request->all();
                $data->update($formData);
                DB::commit();
                Session::flash('success', 'Page updated successfully');
                return ['status' => 'true', 'message' => 'Page updated successfully'];
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 'false', 'message' => $e->getMessage()];
        }
    }
}
