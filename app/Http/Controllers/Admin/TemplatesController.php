<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Template;
use DataTables;
use App\Lib\Helper;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class TemplatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = 'Templates List';
        $breadcrumbs = [
            ['name' => 'Templates', 'relation' => 'Current', 'url' => '']
        ];
        if ($request->ajax()) {
            $templates = Template::all();
            return DataTables::of($templates)
                ->addColumn('action', function ($template) {
                    $template_id = Crypt::encrypt($template->id);
                    return '<a title="Edit" href="' . route('templates.edit', $template_id) . '" class="btn btn-xs btn-info"><i class="fas fa-edit"> Edit</i></a>';
                })
                ->editColumn('type', function ($template) {
                    return ($template->type == 1) ? "SMS" : "Email";
                })
                ->editColumn('status', function ($template) {
                    return Helper::getStatus($template->status, $template->id, route('templates.status'));
                })
                ->editColumn('created_at', function ($template) {
                    return getformattedDate($template->created_at);
                })
                ->rawColumns(['status', 'action', 'type', 'created_at'])
                ->make(true);
        }
        return view('Admin.templates.index', compact('page_title', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $page_title = 'Add Template';
            $breadcrumbs = [
                ['name' => 'Template', 'relation' => 'link', 'url' => route('templates.index')],
                ['name' => 'Add Template', 'relation' => 'Current', 'url' => '']
            ];
            return view('Admin.templates.create', compact('page_title', 'breadcrumbs'));
        } catch (\Throwable $th) {
            return ['status' => 'false', 'message' => $th->getMessage()];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $postdata  = $request->all();
            $rules = [
                'title' => 'required',
                'slug' => 'required',
                'keyword' => 'required',
                'content' => 'required',
            ];
            $validator = Validator::make($postdata, $rules);
            if ($validator->fails()) {
                return response()->json(array('errors' => $validator->messages()), 422);
            } else {
                Template::create($postdata);
                DB::commit();
                Session::flash('success', 'Template Create Successfully');
                return ['status' => 'true', 'message' => 'Template Create Successfully.'];
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            return ['status' => 'false', 'message' => $th->getMessage()];
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decrypt($id);
            $getTemplate = Template::findorFail($id);
            $page_title = 'Edit Template';
            $breadcrumbs = [
                ['name' => 'Template', 'relation' => 'link', 'url' => route('templates.index')],
                ['name' => 'Add Template', 'relation' => 'Current', 'url' => '']
            ];
            //  dd($getTemplate);
            return view('Admin.templates.edit', compact('page_title', 'breadcrumbs', 'getTemplate', 'id'));
        } catch (\Throwable $th) {
            return ['status' => 'false', 'message' => $th->getMessage()];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $getTemplate = Template::findorFail($id);
            $postdata  = $request->all();
            $rules = [
                'title' => 'required',
                'slug' => 'required|unique:templates,slug,' . $id,
                'keyword' => 'required',
                'content' => 'required',
            ];
            $validator = Validator::make($postdata, $rules);
            if ($validator->fails()) {
                return response()->json(array('errors' => $validator->messages()), 422);
            } else {
                $getTemplate->update($postdata);
                DB::commit();
                Session::flash('success', 'Template update Successfully');
                return ['status' => 'true', 'message' => 'Template update Successfully.'];
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            return ['status' => 'false', 'message' => $th->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function status(Request $request)
    {
        DB::beginTransaction();
        try {
            $id = $request->id;
            $row = Template::whereId($id)->first();
            $row->status = $row->status == '1' ? '0' : '1';
            $row->save();
            DB::commit();
            return Helper::getStatus($row->status, $id, route('templates.status'));
        } catch (\Throwable $th) {
            DB::rollBack();
            return ['status' => 'false', 'message' => $th->getMessage()];
        }
    }
}
