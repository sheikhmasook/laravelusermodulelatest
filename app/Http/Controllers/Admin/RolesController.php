<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\Lib\Helper;
use App\Models\Role;
use App\Models\RolePermission;
use Session;
use Validator;
use DataTables;
use Config;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "User Roles";
        $breadcrumbs = [
            ['name' => $title, 'relation' => 'Current', 'url' => ''],
        ];
        if ($request->ajax()) {
            $roles = Role::select(['id', 'name', 'created_at']);
            return DataTables::of($roles)
                ->addColumn('action', function ($role) {
                    $role_id = Crypt::encrypt($role->id);

                    $btn = '<a title="Manage Permissions" href="' . route('roles.permissions', $role_id) . '" class="btn btn-xs btn-primary"><i class="fas fa-user-check"></i></a>&nbsp;';
                    $btn .= '<a title="Edit" href="' . route('roles.edit', $role_id) . '" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></a>&nbsp;';
                    $btn .= '<a data-link="' . route('roles.delete') . '" id="delete_' . $role->id . '" onclick="confirm_delete(' . $role->id . ')" href="javascript:void(0)" title="delete" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i></a>&nbsp;';

                    return $btn;
                })
                ->editColumn('created_at', function ($role) {
                    return getformattedDate($role->created_at);
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('Admin.roles.index', compact('title', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $title = "Create User Roles";
            $breadcrumbs = [
                ['name' => 'User Role', 'relation' => 'link', 'url' => route('roles.index')],
                ['name' => $title, 'relation' => 'Current', 'url' => ''],
            ];
            return view('Admin.roles.create', compact('title', 'breadcrumbs'));
        } catch (\Throwable $th) {
            return ['status' => false, 'message' => $th->getMessage()];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'name' => 'required|max:70|unique:roles,name',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json(array('errors' => $validator->messages()), 422);
            } else {
                $formData = $request->all();
                Role::create($formData);
                DB::commit();
                Session::flash('success', 'Role created successfully.');
                return ['status' => 'true', 'message' => 'Records updated successfully.'];
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $title = "Add New Role";
            $breadcrumbs = [
                ['name' => 'Roles', 'relation' => 'link', 'url' => route('roles.index')],
                ['name' => 'Add New Role', 'relation' => 'Current', 'url' => ''],
            ];
            $id = Crypt::decrypt($id);
            $data = Role::findorFail($id);
            return view('Admin.roles.edit', compact('title', 'breadcrumbs', 'data', 'id'));
        } catch (\Throwable $th) {
            return ['status' => false, 'message' => $th->getMessage()];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'name' => 'required|max:70|unique:roles,name,' . $id,
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json(array('errors' => $validator->messages()), 422);
            } else {
                $data = Role::findorFail($id);
                $formData = $request->all();
                $data->update($formData);
                DB::commit();
                Session::flash('success', 'Record updated successfully.');
                return ['status' => 'true', 'message' => 'Records updated successfully.'];
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request)
    {
        if ($request->ajax() && $request->isMethod('post')) {
            DB::beginTransaction();
            try {
                $id = $request->id;
                $row = Role::whereId($id)->first();
                $row->status = $row->status == 1 ? 0 : 1;
                $row->save();
                DB::commit();
                return Helper::getStatus($row->status, $id, route('admin.admins.status'));
            } catch (\Throwable $th) {
                DB::rollBack();
                return ["status" => "false", "message" => $th->getMessage()];
            }
        }
    }


    public function delete(Request $request)
    {
        if ($request->ajax() && $request->isMethod('post')) {
            $id = $request->id;
            try {
                $delete = Role::where('id', '=', $id)->delete();
                if ($delete) {
                    return ["status" => "true", "message" => "Record Deleted."];
                } else {
                    return ["status" => "false", "message" => "Could not deleted Record."];
                }
            } catch (\Exception $e) {
                return ["status" => "false", "message" => $e->getMessage()];
            }
        }
    }
    public function permissions(Request $request, $role_id)
    {
        DB::beginTransaction();
        try {
            $title = "Role Permissions";
            $breadcrumbs = [
                ['name' => 'Roles', 'relation' => 'link', 'url' => route('roles.index')],
                ['name' => $title, 'relation' => 'Current', 'url' => ''],
            ];
            $permissions = Config::get('params.permissions');
            $role_id = Crypt::decrypt($role_id);
            $entity = RolePermission::firstOrCreate([
                'role_id' => $role_id,
            ]);

            if ($request->ajax() && $request->isMethod('post')) {
                $permissions_keys = [];
                if (!empty($request->permissions)) {
                    $data = $request->permissions;
                    $permissions_keys = array_keys($data);
                }

                $permissions_keys_serialize = serialize($permissions_keys);
                $entity->permissions = $permissions_keys_serialize;
                $entity->save();
                DB::commit();
                Session::flash('success', 'Permissions saved successfully.');
                return ['status' => 'true', 'message' => 'Records updated successfully.'];
            }
            return view('Admin.roles.permissions', compact('title', 'breadcrumbs', 'permissions', 'entity'));
        } catch (\Throwable $th) {
            DB::rollBack();
            return ["status" => "false", "message" => $th->getMessage()];
        }
    }
}
