<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Lib\Email;
use App\Lib\Helper;
use App\Models\Admin;
use App\Models\Role;
use App\Models\Template;
use Auth;
use DataTables;
use Validator;
use Session;
use Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class AdminsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "Backend Users";
        $breadcrumbs = [
            ['name' => $title, 'relation' => 'Current', 'url' => ''],
        ];
        if ($request->ajax()) {
            $admins = Admin::with(['role'])->select(['id', 'name', 'email', 'phone_number', 'role_id', 'status'])->where('role_id', '!=', -1);
            return DataTables::of($admins)
                ->addColumn('action', function ($admin) {

                    $admin_id = Crypt::encrypt($admin->id);
                    $btn = '<a title="Edit" href="' . route('admins.edit', $admin_id) . '" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i></a>&nbsp;';
                    $btn .= '<a data-link="' . route('admins.delete') . '" id="delete_' . $admin->id . '" onclick="confirm_delete(' . $admin->id . ')" href="javascript:void(0)" title="Delete" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i></a>&nbsp;';

                    return $btn;
                })
                ->editColumn('status', function ($admin) {
                    return Helper::getStatus($admin->status, $admin->id, route('admins.status'));
                })
                ->editColumn('role', function ($admin) {
                    return isset($admin->role->name) ? $admin->role->name : '';
                })
                ->editColumn('created_at', function ($admin) {
                    return getformattedDate($admin->created_at);
                })
                ->rawColumns(['status', 'action', 'role'])
                ->make(true);
        }
        return view('Admin.admins.index', compact('title', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $title = "Create Backend User";
            $breadcrumbs = [
                ['name' => 'Backend User', 'relation' => 'link', 'url' => route('admins.index')],
                ['name' => $title, 'relation' => 'Current', 'url' => ''],
            ];
            $roles = Role::getRoleList();
            return view('Admin.admins.create', compact('title', 'breadcrumbs', 'roles'));
        } catch (\Throwable $th) {
            return ['status' => false, 'message' => $th->getMessage()];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'name' => 'required|min:2|max:45',
                'email' => 'required|email|unique:admins,email',
                'phone_number' => 'nullable|unique:admins,phone_number',
                'role_id' => 'required|exists:roles,id'

            ];
            $formData = $request->all();

            $validator = Validator::make($formData, $rules);
            if ($validator->fails()) {

                return response()->json(array('errors' => $validator->messages()), 422);
            } else {
                // dd($formData);
                $password = Helper::randomPassword();
                $password_hash = Hash::make($password);
                $formData['password'] = $password_hash;

                $result = Admin::create($formData);
                DB::commit();
                // $url = route('admin.login');
                // $template = Template::getTemplate('backend-user-registration');
                // $subject = $template->subject;
                // $content = $template->content;
                // $content = str_replace([
                //     '[name]', '[email]', '[password]', '[url]'
                // ], [$result->name, $result->email, $password, $url], $content);

                // $mail_params = ['email' => $result->email, 'subject' => $subject, 'content' => $content];
                // Helper::mailSend($mail_params);

                Session::flash('success', 'Backend user created successfully.');
                return ['status' => 'true', 'message' => 'Records created successfully.'];
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }

        $roles = Role::getRoleList();
        return view('Admin.admins.add', compact('id', 'data', 'title', 'breadcrumbs', 'roles'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $title = "Edit Backend User";
            $breadcrumbs = [
                ['name' => 'Backend User', 'relation' => 'link', 'url' => route('admins.index')],
                ['name' => $title, 'relation' => 'Current', 'url' => ''],
            ];
            $id = Crypt::decrypt($id);
            $roles = Role::getRoleList();
            $data = Admin::findorFail($id);
            return view('Admin.admins.edit', compact('title', 'breadcrumbs', 'roles', 'data', 'id'));
        } catch (\Throwable $th) {
            return ['status' => false, 'message' => $th->getMessage()];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'name' => 'required|min:2|max:45',
                'email' => 'required|email|unique:admins,email,' . $id . ',id,deleted_at,NULL',
                'phone_number' => 'nullable|unique:admins,phone_number,' . $id . ',id,deleted_at,NULL',
                'role_id' => 'required|exists:roles,id'

            ];
            $data = Admin::findorFail($id);
            $formData = $request->all();
            $validator = Validator::make($formData, $rules);
            if ($validator->fails()) {
                return response()->json(array('errors' => $validator->messages()), 422);
            } else {
                $data->update($formData);
                DB::commit();
                Session::flash('success', 'Backend user updated successfully.');
                return ['status' => 'true', 'message' => 'Records updated successfully.'];
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function status(Request $request)
    {
        if ($request->ajax() && $request->isMethod('post')) {
            DB::beginTransaction();
            try {
                $id = $request->id;
                $row = Admin::whereId($id)->first();
                $row->status = $row->status == 1 ? 0 : 1;
                $row->save();
                DB::commit();
                return Helper::getStatus($row->status, $id, route('admins.status'));
            } catch (\Throwable $th) {
                DB::rollBack();
                return ["status" => "false", "message" => $th->getMessage()];
            }
        }
    }


    public function delete(Request $request)
    {
        if ($request->ajax() && $request->isMethod('post')) {
            $id = $request->id;
            try {
                $delete = Admin::where('id', '=', $id)->delete();
                if ($delete) {
                    return ["status" => "true", "message" => "Record Deleted."];
                } else {
                    return ["status" => "false", "message" => "Could not deleted Record."];
                }
            } catch (\Exception $e) {
                return ["status" => "false", "message" => $e->getMessage()];
            }
        }
    }
}
