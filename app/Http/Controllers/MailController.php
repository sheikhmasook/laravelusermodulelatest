<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\EmailJob;
use App\Models\Template;
use App\Models\Setting;


class MailController extends Controller
{
    public function sendMail(Request $request)
    {
        $email = new EmailJob();
        $job = ($email)
            ->delay(now()->addSeconds(2));

        dispatch($job);
        dd("Job dispatched.");
    }
}
