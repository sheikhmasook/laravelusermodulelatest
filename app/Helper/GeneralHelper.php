<?php

use App\Models\Setting;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

function settingCache($key = null)
{
	if ($key) {
		$value = Cache::rememberForever($key, function () use ($key) {
			$check = Setting::where('field_name', $key)->first();
			if ($check) {
				return $check->value;
			} else {
				return '';
			}
		});
		return $value;
	} else {
		return '';
	}
}
if (!function_exists('getformattedDate')) {
	function getformattedDate($date)
	{
		if ($date) {
			return Carbon::parse($date)->format('d M Y');
		} else {
			return 'N/A';
		}
	}
}

function setting($key = null)
{
	if ($key) {
		$setting = Setting::where('field_name', $key)->first();
		if ($setting) {
			return $setting->value;
		} else {
			return '';
		}
	} else {
		return '';
	}
}

function generateReferalCode($name)
{
	$referral_code = strtolower(substr($name, 0, 3));
	$uid = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 5);
	return $referral_code . $uid;
}
if (!function_exists('prd')) {
	function prd($data)
	{
		echo '<pre>';
		print_r($data);
		die;
	}
}
