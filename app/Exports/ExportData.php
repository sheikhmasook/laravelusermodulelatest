<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class ExportData implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public $data;
    public $keyword;

    public function __construct($data, $keyword)
    {
        $this->data = $data;
        $this->keyword = $keyword;
    }


    public function view(): view
    {
        $data =  $this->data;
        return view('Admin.' . $this->keyword . '.excel', compact('data'));
    }
}
