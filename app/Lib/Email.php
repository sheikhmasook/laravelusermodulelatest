<?php

namespace App\Lib;

use Mail;
use App\Models\Template;
use App\Models\Setting;

/**
 * 
 * 
 * This Library use for image upload and resizing.
 *  
 * 
 **/

class Email
{


    public static function emailSend($slug, $data, $email = null, $title = null)
    {
        if (!$email || $email == '') {
            $email           = $data['email'];
        }
        $maildata = Template::where('slug', '=', $slug)->first();
        if ($maildata) {
            //$setting =      Setting::pluck('value','field_name');
            $site_email      = 'tesr@yopmail.com';
            $site_title      = 'User Mangement';
            $message         = str_replace(explode(",", $maildata->keyword), $data, $maildata->content);
            $subject         = ($title == null) ? $maildata->title : $title;

            Mail::send('email.template', array('data' => $message), function ($message) use ($site_email, $email, $subject, $site_title) {
                $message->from($site_email, $site_title);
                $message->to($email, $email);
                $message->subject($subject);
            });
        }
    }
}
