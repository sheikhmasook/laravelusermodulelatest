<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'countries';

	public static function getCountryPhoneCodeList() {
		$data = Country::get();
		$country_list = $phonecode_list = [];
		foreach ($data as $key => $row) {
			$country_list[$row->id] = $row->name;
			$phonecode_list[$row->id] = $row->phonecode;
		}
		return [$country_list, $phonecode_list];
	}
}
