<?php

namespace App\Models;

use App\Notifications\AdminResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppSetting extends Authenticatable
{
    protected $fillable = [
        'android_version', 'ios_version', 'android_force_update', 'ios_force_update', 'android_maintenance', 'ios_maintenance'
    ];
}
