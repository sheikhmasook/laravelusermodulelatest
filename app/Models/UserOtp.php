<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOtp extends Model
{
    //
    protected $table = 'user_otps';

    protected $fillable = ['user_id', 'phone_code', 'otp_code','phone_number'];
}
