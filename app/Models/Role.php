<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'description'
    ];

    public static function getRoleList()
    {
        $list = Role::where(['status' => 1])->pluck('name', 'id')->toArray();
        return $list;
    }
}
